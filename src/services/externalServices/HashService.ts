import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import { User } from '../../models/User';

export const hashPassword = async (password: string): Promise<string> => {
  const passwordHash = await bcrypt.hash(password, 8);

  return passwordHash;
};

export const generateToken = async (requestPassword: string, user: User) => {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  const { _id, name, email, password, adm, isActive } = user;

  const validPasword = await bcrypt.compare(requestPassword, password);

  if (validPasword) {
    const token = jwt.sign(
      { _id, adm, isActive },
      String(process.env.APP_SECRET),
      {
        expiresIn: '1d',
      },
    );

    return {
      _id,
      name,
      isActive,
      password,
      email,
      adm,
      token,
    };
  }

  return false;
};
