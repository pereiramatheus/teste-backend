import _ from "lodash";
import { ObjectId } from "mongoose";
import { User, UserModel } from "../models/User";
import { generateToken, hashPassword } from "./externalServices/HashService";
import { decodeToken } from "../middlewares/Authorization";

// eslint-disable-next-line consistent-return
export const saveUser = async (UserDTO: User) => {
  const { name, email, password, adm } = UserDTO;

  // eslint-disable-next-line @typescript-eslint/no-use-before-define
  const userFound = await findUserByQuery({ email });

  if (!_.isEmpty(userFound)) {
    return {
      ok: false,
      message: "email já cadastrado",
    };
  }

  const hash = await hashPassword(password);

  const doc = new UserModel({
    name,
    email,
    password: hash,
    isActive: true,
    adm,
  });
  await doc.save();
};

export const userLogin = async (
  email: string,
  password: string
): Promise<any> => {
  // eslint-disable-next-line @typescript-eslint/no-use-before-define
  const user: Array<User> = await findUserByQuery({ email });

  if (_.isEmpty(user) || user[0].isActive === false) {
    return {
      message: "usuario não encontrado",
    };
  }

  const loginData = await generateToken(password, user[0]);

  if (!loginData) {
    return { ok: false, message: "email ou sernha inválida" };
  }

  return loginData;
};

export const findUserByQuery = async (query: Object) => {
  const user = await UserModel.find(query);

  return user;
};

export const editUserFields = async (
  userIdFromToken: ObjectId,
  query: User
) => {
  const users = await findUserByQuery({ _id: userIdFromToken });

  if (_.isEmpty(users)) {
    return {
      message: "usuario não encontrado",
    };
  }

  const [user]: any = users;

  const { name, password, email, isActive } = query as any;

  if (user.isActive === false) {
    return {
      ok: false,
      data: "apenas um admnistrador pode reativar o usuário",
    };
  }

  if (isActive === "true") {
    return {
      ok: false,
      data: "usuário já ativo",
    };
  }

  if (isActive === "false") {
    user.isActive = false;
  }

  if (name) {
    user.name = name;
  }

  if (password) {
    const hash = await hashPassword(password);

    if (!hash) {
      return {
        ok: false,
        data: "erro ao atualizar password",
      };
    }
    user.password = hash;
  }

  if (email) {
    user.email = email;
  }

  user.save();

  return {
    ok: true,
    data: user,
  };
};

export const reactiveUser = async (userId: string, auth: any): Promise<any> => {
  const tokenDecoded = await decodeToken(auth);

  if (!tokenDecoded) {
    return {
      ok: false,
      data: "token inválido",
    };
  }

  const { adm, isActive } = tokenDecoded;

  if (!adm || !isActive ) {
    return {
      ok: false,
      data: "token apenas administradores ativos pode reativar usuários",
    };
  }

  const users: Array<User> = await findUserByQuery({ _id: userId });

  if (_.isEmpty(users)) {
    return {
      ok: false,
      data: "usuario não encontrado",
    };
  }

  const [user]: any = users;

  if (user.isActive) {
    return {
      ok: false,
      data: "usuário já ativo",
    };
  }

  user.isActive = true;
  user.save();

  return {
    ok: true,
    data: user,
  };
};
