import _ from 'lodash';
import { decodeToken } from '../middlewares/Authorization';
import { Movie, MovieModel } from '../models/Movie';
import { toObject } from '../middlewares/utils/fnUtils';
import { Rate, RateModel } from '../models/Rate';

export const saveMovie = async (
  movieDTO: Movie,
  authrization: any,
): Promise<any> => {
  const { title, actors, director, gender } = movieDTO;

  const tokenDecoded = await decodeToken(authrization);

  const { isActive, adm }: any = tokenDecoded;

  if (!isActive || !adm) {
    return {
      ok: false,
      message: 'apenas usuários ativos podem avaliar filmes',
    };
  }

  if (!tokenDecoded) {
    return {
      ok: false,
      message: 'erro na autenticação',
    };
  }

  // eslint-disable-next-line @typescript-eslint/no-use-before-define
  const movieFound = await findMovieByQuery({ title });

  if (!_.isEmpty(movieFound)) {
    return {
      ok: false,
      message: 'filme já cadastrado',
    };
  }

  const doc = new MovieModel({
    title,
    actors,
    director,
    gender,
  });
  await doc.save();
  return {
    ok: true,
    message: 'cadastrado com sucesso',
  };
};

export const searchMovies = async (movieDTO: Movie) => {
  const { title, actors, gender, director } = movieDTO;

  const query: any = {};

  if (title) {
    query.title = { $regex: `^${title}` };
  }

  if (director) {
    query.director = { $regex: `^${director}` };
  }

  if (gender) {
    query.gender = { $regex: `^${gender}` };
  }

  if (actors) {
    query.actors = { $all: actors };
  }

  const queryObj = toObject(query);

  // eslint-disable-next-line @typescript-eslint/no-use-before-define
  const result = await findMovieByQuery(queryObj);

  return result;
};

export const findMovieByQuery = async (query: Object) => {
  const movies = await MovieModel.find(query);

  return movies;
};

// eslint-disable-next-line consistent-return
export const rateMovie = async (movieId: any, rateDTO: Rate) => {
  const { score, comment } = rateDTO;

  const movieFound = await findMovieByQuery({ _id: movieId });

  if (_.isEmpty(movieFound)) {
    return {
      message: 'filme não encontrado',
    };
  }

  const rateCrated = new RateModel({
    movieId,
    score,
    comment,
    isActive: true,
  });

  rateCrated.save();
};
