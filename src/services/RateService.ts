import _ from "lodash";
import { ObjectId } from "mongoose";
import { decodeToken } from "../middlewares/Authorization";
import { Rate, RateModel } from "../models/Rate";
import { findMovieByQuery } from "./MovieService";

export const createRate = async (
  movieId: any,
  rateDTO: Rate,
  authorization: any
) => {
  const { score, comment } = rateDTO;

  const tokenDecoded = await decodeToken(authorization);

  if (!tokenDecoded) {
    return {
      ok: false,
      message: "erro na autenticação",
    };
  }

  const  {adm, isActive}: any = tokenDecoded

  if (adm || !isActive) {
    return {
      ok: false,
      data: "apenas usuários ativos não admnisitradores pode avaliar",
    };
  }

  const movieFound = await findMovieByQuery({ _id: movieId });

  if (_.isEmpty(movieFound)) {
    return {
      ok: false,
      data: "filme não encontrado",
    };
  }

  const [movie]: any = movieFound;

  const rateCrated = new RateModel({
    movieId,
    score,
    comment,
    isActive: true,
  });

  const rate = await rateCrated.save();

  // eslint-disable-next-line @typescript-eslint/no-use-before-define
  const avg = await averageMovie(movieId);

  if (avg) {
    movie.average = avg;
    movie.save();
  }

  return {
    ok: true,
    data: rate,
  };
};

export const findRate = async (rateId: ObjectId) => {
  const rate = await RateModel.findOne({ _id: rateId });

  return rate;
};

export const findRateByQuery = async (query: Object) => {
  const movies = await RateModel.find(query);

  return movies;
};

export const averageMovie = async (
  movieId: ObjectId
): Promise<number | false> => {
  const rates = await findRateByQuery({ movieId });

  if (_.isEmpty(rates)) {
    return false;
  }

  let scoreSum = 0;
  let avgCount = 0;

  rates.forEach((rate: Rate) => {
    if (rate.movieId === movieId && rate.isActive === true) {
      scoreSum += rate.score;
     
      avgCount += 1;
    }
  });



  const average = scoreSum / avgCount;

  return average;
};

export const unactivateRate = async (
  rateId: string | ObjectId,
  authorization: any
) => {
  const rates = await findRateByQuery({ _id: rateId });

  const admin = await decodeToken(authorization);

  if (!admin) {
    return {
      ok: false,
      data: "apenas usuários admnisitradores podem excluir avaliações ",
    };
  }

  if (_.isEmpty(rates)) {
    return false;
  }

  const [element]: any = rates;

  if (!element.isActive) {
    return {
      ok: false,
      data: "avaliação já excluída ",
    };
  }

  const rate = element;
  rate as Rate;
  rate.isActive = false;
  rate.save();

  const movies = await findMovieByQuery({ _id: rate.movieId });


  if (_.isEmpty(movies)) {
    return {
      ok: false,
      data: "erro ao atualizar avaliação do filme",
    };
  }
  
  const [movie]: any = movies;

  const avg = await averageMovie(rate.movieId);

  movie.average = avg;
  movie.save();

  return {
    ok: true,
    data: rate,
  };
};
