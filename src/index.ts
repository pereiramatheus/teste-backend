import express from "express";
import { connectToDatabase } from "./database/Mongo";
import './config/config'
import routes from "./routes";

const app = express();
const port = process.env.PORT;
const databse =  String(process.env.DATABSE )
const mongoUri = String(process.env.MONGO_URI)

app.use(routes);
app.listen(port, () => {
  connectToDatabase(mongoUri, databse);
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  // eslint-disable-next-line no-console
  console.log(`listening on port ${port} 🚀`);
});
