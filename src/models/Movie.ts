import { model, ObjectId, Schema } from "mongoose";

export interface Movie {
  _id?: ObjectId;
  title: string;
  actors: Array<string>;
  director: string;
  gender: string;
  average: number
}

const schema = new Schema<Movie>({
  title: { type: String, required: true },
  actors: { type: Array, required: true },
  director: { type: String, required: true },
  gender: { type: String, required: true },
  average: { type: Number, required: false },
});

export const MovieModel = model<Movie>("Movie", schema);
