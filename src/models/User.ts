import { model, ObjectId, Schema } from "mongoose";

export interface User {
  _id?: ObjectId;
  name: string;
  email: string;
  password: string;
  adm: boolean;
  isActive: boolean;
}

const schema = new Schema<User>({
  name: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  adm: { type: Boolean, required: true },
  isActive: { type: Boolean, required: false },
});

export const UserModel = model<User>("User", schema);
