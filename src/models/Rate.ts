import { model, ObjectId, Schema } from "mongoose";

export interface Rate {
  _id?: ObjectId;
  score: number;
  movieId: ObjectId | string
  comment?: string;
  isActive?: boolean;
}

const schema = new Schema<Rate>({
  movieId: { type: String, required: true },
  score: { type: Number, required: true },
  comment: { type: Number, required: false },
  isActive: { type: Boolean, required: false },
});

export const RateModel = model<Rate>("Rate", schema);
