/* eslint-disable @typescript-eslint/no-unused-vars */
import { connect } from "mongoose";

// eslint-disable-next-line import/prefer-default-export
export const connectToDatabase = async (uri: string, databaseName: string) => {
  await connect(`${uri}/${databaseName}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
    .then(() => {
      // eslint-disable-next-line no-console
      console.log("mongo connected 💾");
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log("Error to connect to databse");
    });
};
