import * as jwt from 'jsonwebtoken';
import { User } from '../models/User';

export const decodeToken = async (auth: any) => {
  try {
    const { authorization } = auth;

    const [, token] = authorization.split(' ');

    const tokenDecoded = jwt.verify(token, String(process.env.APP_SECRET));

    // eslint-disable-next-line @typescript-eslint/naming-convention
    const { adm, _id, isActive }: any = tokenDecoded;

    return {
      adm,
      _id,
      isActive,
    };
  } catch (err) {
    return false;
  }
};

export const getIdFromToken = async (auth: any) => {
  try {
    const { authorization } = auth;

    const [, token] = authorization.split(' ');

    const tokenDecoded = jwt.verify(token, String(process.env.APP_SECRET));

    // eslint-disable-next-line @typescript-eslint/naming-convention
    const { _id } = tokenDecoded as User;

    return _id;
  } catch (err) {
    return false;
  }
};
