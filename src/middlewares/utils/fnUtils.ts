/* eslint-disable import/prefer-default-export */
export const toObject = (objThis: any): any => {
  try {
    const obj = { ...objThis };

    // eslint-disable-next-line array-callback-return
    Object.keys(obj).map((key) => {
      if (obj[key] === undefined) {
        delete obj[key];
      }
    });

    return obj;
  } catch (ex) {
    return objThis;
  }
};
