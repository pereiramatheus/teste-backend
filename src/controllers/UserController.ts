import {  Request, Response } from "express";
import { decodeToken } from "../middlewares/Authorization";
import { User } from "../models/User";
import {
  editUserFields,
  reactiveUser,
  saveUser,
  userLogin,
} from "../services/UserService";

export const save = async (req: Request, res: Response): Promise<any> => {
  const user = await saveUser(req.body);

  return res.json(user);
};

export const login = async (req: Request, res: Response): Promise<any> => {
  const { email, password } = req.body;

  const user = await userLogin(email, password);

  return res.json(user);
};

export const editUser = async (req: Request, res: Response): Promise<any> => {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  const { _id }: any = await decodeToken(req.headers);

  if (!_id) {
    return { ok: false, data: res.status(400) };
  }

  const queryPlayload = req.query as unknown as User;

  const result = await editUserFields(_id, queryPlayload);

  return res.json(result);
};

export const activeUser = async (req: Request, res: Response): Promise<any> => {
  const { userId } = req.params;

  const user = await reactiveUser(userId, req.headers);

  return res.json(user);
};
