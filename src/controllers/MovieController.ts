import { Request, Response } from 'express';
import { saveMovie, searchMovies } from '../services/MovieService';
import { Movie } from '../models/Movie';
import {
  createRate,
  findRateByQuery,
  unactivateRate,
} from '../services/RateService';

export const addMovie = async (req: Request, res: Response): Promise<any> => {
  const result = await saveMovie(req.body, req.headers);

  return res.json( result.message );
};

export const listMovies = async (req: Request, res: Response): Promise<any> => {
  const query = req.query as unknown as Movie;

  const result = await searchMovies(query);

  return res.json(result);
};

export const avaliateMovie = async (req: Request, res: Response) => {
  const { movieId }: any = req.params;

  const result = await createRate(movieId, req.body, req.headers);

  return res.json(result);
};

export const listRatesByMovieId = async (req: Request, res: Response) => {
  const { movieId }: any = req.params;

  const result = await findRateByQuery({ _id: movieId });

  return res.json(result);
};

export const listRates = async (req: Request, res: Response) => {
  const result = await findRateByQuery({ isActive: true });

  return res.json(result);
};

export const deleteRate = async (req: Request, res: Response) => {
  const { rateId }: any = req.params;

  const result = await unactivateRate(rateId, req.headers);

  return res.json(result);
};
