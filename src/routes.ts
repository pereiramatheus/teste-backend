import express, { Router } from "express";
import { addMovie, avaliateMovie, deleteRate, listMovies, listRates } from "./controllers/MovieController";
import { activeUser, editUser, login, save } from "./controllers/UserController";

const routes = Router();

routes.use(express.json());
routes.use(express.urlencoded());

routes.get("/movies", listMovies);
routes.get("/rates", listRates);
routes.post("/movies/delete-rate/:rateId", deleteRate);
routes.post("/movies/rate-movie/:movieId", avaliateMovie)
routes.post("/user/create-user", save);
routes.post("/user/active-user/:userId", activeUser)
routes.post("/user/login", login);
routes.patch("/user/edit-user", editUser);
routes.post("/movie/create-movie", addMovie);


export default routes;
